from fastapi import status
from fastapi.responses import JSONResponse
import json
from fastapi_utils.inferring_router import InferringRouter
from pydantic import EmailStr

from utils.database import db
from models.ProfileModel import ProfileModel
from schemas.ProfileSchema import ProfileSchema, ProfileListSchema

router = InferringRouter()


class ProfileController:

    @router.post("/profile/create")
    def create_profile(profile: ProfileSchema):

        profile_new = ProfileModel(form_academica=profile.form_academica,
                                   estado_civil=profile.estado_civil,
                                   educacion=profile.educacion,
                                   vivienda=profile.vivienda,
                                   skills=profile.skills,
                                   owner_email=profile.owner_email)

        if db.add(profile_new):

            response = {"status": "success"}
            return JSONResponse(content=response, status_code=status.HTTP_201_CREATED)

        response = {"status": "error"}
        return JSONResponse(content=response, status_code=status.HTTP_206_PARTIAL_CONTENT)

    @router.get("/profiles", response_model=ProfileListSchema)
    def get_profiles():
        profiles = db.query(ProfileModel).all()

        if profiles is None:
            response = {"msg": "usuario no encontrado"}

            return JSONResponse(content=response, status_code=status.HTTP_204_NO_CONTENT)
        
        result = []
        for profile in profiles:
            result.append(profile._dump())
        response = {"profiles" : result}

        return JSONResponse(content=response, status_code=status.HTTP_200_OK)

    @router.get("/profile", response_model=ProfileSchema)
    def get_profile_by_email(email: EmailStr):

        profile = db.query(ProfileModel).filter(f"owner_email=='{email}'").first()

        response = {
            "form_academica" : profile.form_academica,
            "estado_civil" : profile.estado_civil,
            "educacion" : profile.educacion,
            "vivienda" : profile.vivienda,
            "owner_email" : profile.owner_email,
            "skills" : [profile.skills]
        }

        return JSONResponse(content=response, status_code=status.HTTP_200_OK)
