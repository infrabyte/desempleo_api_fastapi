import re
from fastapi import status
from fastapi.responses import JSONResponse
from fastapi_utils.inferring_router import InferringRouter
import bcrypt
from utils.database import db
from models.UserModel import UserModel
from schemas.UserSchema import UserSchema, UserSchemaIn ,UserListSchema, LoginSchema, UserSchemaUpdate

router = InferringRouter()

class UserController:

    @router.get("/users", response_model=UserListSchema)
    def GetAll():
        # users = db.query(UserModel).returns('_key', 'name', 'email').all()
        users = db.query(UserModel).all()
        # print(users.profile)
        result = []
        for user in users:
            result.append(user._dump())
        response = {"users" : result}

        return JSONResponse(content=response, status_code=status.HTTP_200_OK)
    
    @router.post("/users/create", response_model=UserSchema)
    def create_user(user: UserSchemaIn):
        passw = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
        user_new = UserModel(name=user.name, email=user.email, password = passw)
        
        db.add(user_new)

        return JSONResponse(content=user, status_code=status.HTTP_201_CREATED)

    @router.delete("/user/delete")
    def delete_user(email):
        get_user = db.query(UserModel).filter("email==@email", email=email).first()
        print(get_user._key)
        if get_user is None:
            response = {"msg" : "usuario no encontrado"}

            return JSONResponse(content=response, status_code=status.HTTP_204_NO_CONTENT)
        
        if db.query(UserModel).filter("email==@email", email=email).delete():

            response = {"status" : "success"}

            return JSONResponse(content=response, status_code=status.HTTP_200_OK)
    
    @router.put("/user/update")
    def update_user(user: UserSchemaUpdate):
        get_user = db.query(UserModel).filter("email==@email", email=user.email).first()
        print(get_user._key)
        if get_user is None:
            response = {"msg" : "usuario no encontrado"}

            return JSONResponse(content=response, status_code=status.HTTP_204_NO_CONTENT)

        passw = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
        db.query(UserModel).filter("email==@email", email=user.email).update(
            email = user.email_update,
            name = user.name,
            password = passw
        )

        response = {"status" : "success"}

        return JSONResponse(content=response, status_code=status.HTTP_200_OK)

    @router.post("/users/login")
    def login(user: LoginSchema):
        get_user = db.query(UserModel).filter("email==@email", email=user.email).first()
        passw = bcrypt.checkpw(user.password.encode('utf-8'), get_user.password.encode('utf-8'))
        print(get_user.email)
        if get_user is None:
            response = {"msg" : "usuario no encontrado"}

            return JSONResponse(content=response, status_code=status.HTTP_204_NO_CONTENT)

        if passw is False:
            response = {"msg" : "la contraseña es incorrecta"}

            return JSONResponse(content=response, status_code=status.HTTP_204_NO_CONTENT)
        
        response = {"status" : "success"}

        return JSONResponse(content=response, status_code=status.HTTP_200_OK)
