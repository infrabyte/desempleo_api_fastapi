import logging

from fastapi import FastAPI

from utils import settings
from utils.database import db
from routes import UserRouter, ProfileRouter
from models.UserModel import UserModel
from models.ProfileModel import ProfileModel

import uvicorn

log = logging.getLogger(__name__)

def on_startup():
    """Fastapi startup event handler.
    Creates AiohttpClient session.
    """
    log.debug("Execute FastAPI startup event handler.")

def on_shutdown():
    """Fastapi shutdown event handler.
    Destroys AiohttpClient session.
    """
    log.debug("Execute FastAPI shutdown event handler.")


if __name__ == "__main__":
    log.debug("Initialize FastAPI application node.")
    app = FastAPI(
        title=settings.PROJECT_NAME,
        debug=settings.DEBUG,
        version=settings.VERSION,
        docs_url="/",
        on_startup=[on_startup],
        on_shutdown=[on_shutdown],
    )

    log.debug("Add application routes.")
    app.include_router(UserRouter.router)
    app.include_router(ProfileRouter.router)

    db.create_all([UserModel, ProfileModel])

    uvicorn.run(app=app, debug='true')