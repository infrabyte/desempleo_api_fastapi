from arango_orm import Collection
from arango_orm.references import relationship
from arango_orm.fields import String, List, Nested
from models.UserModel import UserModel
from schemas.ProfileSchema import SkillSchema

# class SkillSchema(BaseModel):
#     skill : str

class ProfileModel(Collection):
    __collection__  = 'profile'

    form_academica = String(required=True)
    estado_civil = String(required=True)
    educacion = String(required=True)
    vivienda = String(required=True)
    skills = List(String())
    owner_email = String(required=True)

    owner = relationship(UserModel, 'owner_email', cache=False)