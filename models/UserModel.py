from arango_orm import Collection
from arango_orm.references import relationship
from arango_orm.fields import String, Date

class UserModel(Collection):
    __collection__  = 'users'
    # _index = [{'type': 'hash', fields: ['email'], unique=True}]
    
    name = String(required=True)
    email = String(required=True)
    password = String(required=True)

    profile = relationship(__name__ + ".ProfileModel", 'email', target_field='owner_email')