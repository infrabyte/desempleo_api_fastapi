from fastapi import APIRouter
from controllers import ProfileController

router = APIRouter (
    prefix="/api"
)

router.include_router(ProfileController.router, tags=["profile"])