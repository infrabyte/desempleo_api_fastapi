from pydantic import BaseModel, EmailStr
from typing import List

class SkillSchema(BaseModel):
    skill : str

class ProfileSchema(BaseModel):
    
    form_academica : str
    estado_civil : str
    educacion : str
    vivienda : str
    skills : List[str]
    owner_email : EmailStr

class ProfileListSchema(BaseModel):
    profiles : List[ProfileSchema]