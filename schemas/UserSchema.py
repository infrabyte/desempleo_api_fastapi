from pydantic import BaseModel, EmailStr
from typing import List

class UserSchema(BaseModel):
    _key : str
    name: str
    password: str
    email: EmailStr

class UserSchemaIn(BaseModel):
    name: str
    password: str
    email: EmailStr

class UserSchemaUpdate(BaseModel):
    name: str
    password: str
    email: EmailStr
    email_update: EmailStr

class UserListSchema(BaseModel):
    users : List[UserSchema]

class LoginSchema(BaseModel):
    email: str
    password: str