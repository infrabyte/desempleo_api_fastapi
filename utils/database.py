from arango import ArangoClient
from arango_orm import Database

client = ArangoClient(hosts='http://127.0.0.1:8529')

sys_db = client.db('_system', username='root', password='wil99')

# Check if task database exist, create one if doesnt exist
if not sys_db.has_database('desempleodb'):
    sys_db.create_database('desempleodb')

desempleodb_client = client.db('desempleodb', username='root', password='wil99')

db = Database(desempleodb_client)